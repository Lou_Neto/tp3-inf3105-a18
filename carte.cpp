/*  INF3105 - Structures de données et algorithmes
    UQAM | Faculté des sciences | Département d'informatique
    TP3
    Fichier de départ carte.cpp © Éric Beaudry (beaudry.eric@uqam.ca).
    http://ericbeaudry.ca/INF3105/tp3/

    Lou-Gomes Neto (NETL14039105)
*/
#include <cmath>
#include <cstdio>
#include <limits>
#include <queue>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <utility>
#include <string>
#include "carte.h"

void Carte::ajouter_noeud(long id, const PointST& p){
    noeuds[id].point = p;
}

void Carte::ajouter_route(const std::string& nomroute, 
                          const std::list<long>& route){

    std::list<long>::const_iterator origin = route.begin();
    std::list<long>::const_iterator dest = route.begin();
    dest++;
    while(dest != route.end())
    {
        noeuds[*origin].voisins.insert(*dest);
        origin++;
        dest++;
    }
    nomRoutes.push_back(nomroute);
    routes.push_back(route);
}

/////////////////////  PARTIE A /////////////////////////////
double Carte::calculerChemin(const std::string& nomelecteur, 
                             const std::string& nomorigine, 
                             const std::string& nomdestination,
                             std::list<std::string>& out_cheminnoeuds, 
                             std::list<std::string>& out_cheminroutes)
{
    // recuperer indices de l'origine et destination
    long origine = atol(nomorigine.c_str()+1);
    long destination = atol(nomdestination.c_str()+1);

    // Dijkstra
    std::vector<double> distances;
    std::vector<long> parents;
    
    std::map<long, Noeud>::const_iterator iter = noeuds.begin();
    for(; iter != noeuds.end(); iter++) {
        distances.push_back(std::numeric_limits<double>::infinity());
        parents.push_back(-1);
    }
    distances[origine] = 0;

    std::priority_queue< std::pair<double, long>, std::vector<std::pair<double, long> >, compare > q;
    std::pair<double, long> p = std::make_pair(0, origine);
    q.push(p);

    while(!q.empty()) {

        std::pair<double, long> v = q.top();
        q.pop();

        if(distances[v.second] == std::numeric_limits<double>::infinity())
            break;

        std::set<long>::const_iterator voisin = noeuds[v.second].voisins.begin();
        for(; voisin != noeuds[v.second].voisins.end(); voisin++){
            double d = distances[v.second] + distance(noeuds[v.second].point, noeuds[*voisin].point); 

            if(d < distances[*voisin]) {
                parents[*voisin] = v.second;
                distances[*voisin] = d;
                std::pair<double, long> w = std::make_pair(d, *voisin);
                q.push(w);
            }
        }
    }

    // preparation de l'affichage
    long l = destination;
    std::list<long> parcours;
    while(l != origine) {
        parcours.push_front(l);
        char n[20];
        sprintf(n,"n%ld",l);
        out_cheminnoeuds.push_front(n);
        l = parents[l];
    }
    char n[20];
    sprintf(n,"n%ld",origine);
    out_cheminnoeuds.push_front(n);

    // std::list<long>::const_iterator iterParcours = parcours.begin();
    // std::list<long>::const_iterator iterParcours2 = parcours.begin();
    // iterParcours2++;
    // while(iterParcours2 != parcours.end()) {
    //     int i = 0;
    //     std::vector<std::list<long> >::const_iterator r = routes.begin();
    //     for(; r != routes.end(); r++) {
    //         std::list<long>::const_iterator iter = r->begin();
    //         std::list<long>::const_iterator iter2 = r->begin();
    //         iter2++;
    //         if(*iter == *iterParcours && *iter2 == *iterParcours2) {
    //             out_cheminroutes.push_front(nomRoutes[i]);
    //         }
    //         i++;
    //         iter++;
    //         iter2++;
    //     }
    //     iterParcours++;
    //     iterParcours2++;
    // }

    out_cheminroutes.push_back("(noms de rues non trouvees)");

    // retourner l'affichage
    return distances[destination];
}

/////////////////////  PARTIE B /////////////////////////////
int Carte::calculerPancartes(int nbPancartes, std::vector<std::pair<std::string, std::string> >& segmentsroutes){
    // À compléter.
    for(int i=0;i<nbPancartes;i++)
        segmentsroutes[i] = std::make_pair<std::string, std::string>("n1", "n2");
    return 0;
}