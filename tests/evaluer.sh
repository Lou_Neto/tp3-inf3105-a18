#!/bin/bash
##########################################################################
# UQAM - Département d'informatique
# INF3105 - Structures de données et algorithmes
# Automne 2018
# TP3 - Graphes / Cartes / Pancartes électorales
# http://ericbeaudry.uqam.ca/INF3105/tp3/
# beaudry.eric@uqam.ca
#
# Script d'évaluation
#
# Instructions:
# 1. Déposer ce script avec les fichiers problèmes dans un répertoire
#    distinct (ex: tests).
# 2. Se placer dans le répertoire contenant votre programme ou contenant
#    la liste des soumissions Oto (*.tp_oto).
# 3. Lancer ce script (ex: ../tests/evaluer.sh).
##########################################################################

# Obtenir le chemin du répertoire contenant le présent script et les fichiers tests
pushd `dirname $0`
repertoire_tests=`pwd`
# Cartes à tester
cartes="uqam stdamas sthilaire montreal"
popd

echo "UQAM | Département d'informatique"
echo "INF3105 | Structures de données et algorithmes"
echo "Évaluation du TP3"
echo

if [ `pwd` -ef $repertoire_tests ];
then
    echo "Ce script doit être dans un répertoire différent de celui contenant votre tp2."
    echo "Ce script a été arrêté afin de ne pas écraser les fichiers test_[CDG]*.resultat!"
    exit -2;
fi

# Le programme à tester
programmeA=tp3a
programmeB=tp3b

# Vérification de la présence des programmes à évaluer
if [ ! -e ${programmeA} ]
	then
	echo "  ERREUR : le fichier ${programmeA} est inexistant!"
	exit -1
fi

if [ ! -x ${programmeA} ]
	then
	echo "  ERREUR : le fichier ${programmeA} n'est pas exécutable!"
	exit -1
fi

if [ ! -e ${programmeB} ]
	then
	echo "  ERREUR : le fichier ${programmeB} est inexistant!"
	exit -1
fi

if [ ! -x ${programmeB} ]
	then
	echo "  ERREUR : l'fichier ${programmeB} n'est pas exécutable!"
	exit -1
fi

# Détection des valideurs.
valideurA="${repertoire_tests}/valideurA"
valideurB="${repertoire_tests}/valideurB"

if [ -x "${valideurA}-`uname`-`uname -p`" ]
then
  valideurA="${valideurA}-`uname`-`uname -p`"
fi
if [ -x "${valideurB}-`uname`-`uname -p`" ]
then
  valideurB="${valideurB}-`uname`-`uname -p`"
fi

if [ ! -x ${valideurA} ]
then
   echo "Avertissement : le valideurA n'est pas disponible!"
fi
if [ ! -x ${valideurB} ]
then
   echo "Avertissement : le valideurB n'est pas disponible!"
fi

# Détection si l'utilitaire time sous Linux est disponible (peut afficher 0.00)
echo "Détection de /usr/bin/time..."
/usr/bin/time -f %U echo 2>&1 > /dev/null
souslinux=$?


# Limiter le temps d'exécution et la quantite de mémoire
#ulimit -t unlimited -v 2097152 -f 20480
ulimit -t 90 -v 2097152 -f 20480
echo "ulimit (limites courantes des ressources) :"
ulimit -t -v -f
echo "-----"


date=`date +%Y%m%d_%H%M%S`

# Détection du CPU
if [ -e /proc/cpuinfo ] ; then
    cpuinfo=`grep "model name" /proc/cpuinfo | sort -u | cut -d ":" -f 2`
else
    cpuinfo="?"
fi

function Nettoyer
{
    echo "Nettoyage"
    # Au cas où le Makefile des étudiants ne ferait pas un «make clean» correctement.
    #make clean

    rm -f *.o* *.gch tp[1-3]
    # Au besoin, nettoyer les précédents fichiers logs
    rm -f log*.txt
}


## La fonction qui évalue un TP.
function EvaluerTP 
{
    # Au cas où le Makefile soumis ne faisait pas un «make clean» correctement.
    #rm -f *.o* *.gch tp[1-3][ab] *+{A,B[1-3]}.txt
    #make clean
    # Au besoin, nettoyer les précédents fichiers logs
    #rm -f log*.txt

    logfile="log-${date}.txt"

    # Extraction des codes permanents dans les fichiers sources
    echo "Rapport de correction de : " > ${logfile}
    ( (pwd | grep -oh [a-zA-Z]{4}[0-9]{8}) && grep -oh  [a-zA-Z]{4}[0-9]{8} *.{h*,c*} ) | sort -u >> ${logfile}
    echo -e "\n\n" >> ${logfile}

    # Pour statistiques : nombre de lignes de code...
    echo "Taille des fichiers source :" >> ${logfile}
    wc *.{c*,h*}  >> ${logfile}

    taille=`wc *.{c*,h*} | grep total`
    checkcopie=`cat *.h | grep -o "H__" | wc -l`
    checkSTL=`cat *.h | grep -o -E "<set>|<map>|<vector>|<list>" | wc -l`
    sommaire="${sommaire}$checkcopie\t$checkSTL\t$taille"

    echo -e "\nCompilation ..."
    echo -e "\nCompilation ..." >> ${logfile}

    if [ ! -e Makefile ]; then
        echo "ERREUR : Makefile inexistant!"
        echo "ERREUR : Makefile inexistant!" >> ${logfile}
        return;
    fi

    #make clean
    echo -ne
    make tp3a tp3b 2>&1 >> ${logfile}
    echo -ne

    if [ ! -x tp3a ]; then
        echo "ERREUR : l'executable tp3a n'a pas été produit!"
        echo "ERREUR : l'executable tp3a n'a pas été produit!" >> ${logfile}
        return
    fi

    if [ ! -x tp3b ]; then
        echo "Avertissement : l'executable tp3b n'a pas été produit!"
        echo "Avertissement : l'executable tp3b n'a pas été produit!" >> ${logfile}
    fi

    echo "Évaluation des temps d'exécution de tp3a et tp3b sur les jeux de tests."
    echo "Les résultats sont déposés dans $logfile."


    echo "Machine : " `hostname` "."
    echo "#Machine : " `hostname` "."  > $logfile
    echo "CPU :$cpuinfo"
    echo "#CPU :$cpuinfo"  >> $logfile
    echo "Date début : $date."
    echo "#Date début : $date."  >> $logfile
    echo "Limite de `ulimit -t` secondes par test."
    echo "#Limite de `ulimit -t` secondes par test."  >> $logfile
    echo 
    echo "#" >> $logfile

    echo -e "#Liste_Test\tA_CPU\tA_NbOpt\tTaille\tB1_CPU\tB1_Nb\tB2_CPU\tB2_Nb\tB3_CPU\tB3_Nb" >> $logfile
    echo -e  "Liste_Test\tA_CPU\tA_NbOpt\tTaille\tB1_CPU\tB1_Nb\tB2_CPU\tB2_Nb\tB3_CPU\tB3_Nb"

    # Itérer sur toutes les cartes
    for carte in $cartes;
    do
        fichiercarte="${repertoire_tests}/${carte}-carte.txt"

        # Lister les fichiers test
        tests="`cd $repertoire_tests && ls ${carte}-electeurs[0-9].txt`"

        # Itérer sur tous les fichiers test
        for test in $tests;
        do
            nblignes=`cat $repertoire_tests/$test | wc -l`
            
            if [ $souslinux -eq 0 ]; then
            tA=`(/usr/bin/time -f "%U" ./tp3a $fichiercarte $repertoire_tests/$test > ${test%.txt}+A.txt) 2>&1 | tail -n 1`
            tB1=`(/usr/bin/time -f "%U" ./tp3b $fichiercarte 1 $repertoire_tests/$test > ${test%.txt}+B1.txt) 2>&1 | tail -n 1`
            tB2=`(/usr/bin/time -f "%U" ./tp3b $fichiercarte 2 $repertoire_tests/$test > ${test%.txt}+B2.txt) 2>&1 | tail -n 1`
            tB3=`(/usr/bin/time -f "%U" ./tp3b $fichiercarte 3 $repertoire_tests/$test > ${test%.txt}+B3.txt) 2>&1 | tail -n 1`
            else
            tA=`(time -p  ./tp3a $fichiercarte $repertoire_tests/$test > ${test%.txt}+A.txt) 2>&1 | egrep user | cut -f 2 -d " "`
            tB1=`(time -p  ./tp3b $fichiercarte 1 $repertoire_tests/$test > ${test%.txt}+B1.txt) 2>&1 | egrep user | cut -f 2 -d " "`
            tB2=`(time -p  ./tp3b $fichiercarte 2 $repertoire_tests/$test > ${test%.txt}+B2.txt) 2>&1 | egrep user | cut -f 2 -d " "`
            tB3=`(time -p  ./tp3b $fichiercarte 3 $repertoire_tests/$test > ${test%.txt}+B3.txt) 2>&1 | egrep user | cut -f 2 -d " "`
            fi
            
            validA="?"
            validB1="?"
            validB2="?"
            validB3="?"
            
            if ( [ -e ${repertoire_tests}/${test%.txt}+A.txt ] ) ; then
                if( [ -x ${valideurA} ] ) ; then
	                validA=`$valideurA $fichiercarte ${repertoire_tests}/${test} ${test%.txt}+A.txt`
	            else
	                diff -tibw "${repertoire_tests}/${test%.txt}+A.txt" "${test%.txt}+A.txt" 2>&1 > /dev/null
                    if [ $? -eq 0 ]; then validA="=="; else validA="!="; fi
                fi
            fi
            if ( [ -e ${repertoire_tests}/${test%.txt}+A.txt ] && [ -e  ${repertoire_tests}/${test%.txt}+B1.txt ] ) ; then
                if( [ -x ${valideurB} ] ) ; then
	                validB1=`$valideurB -c ${repertoire_tests}/${test} ${repertoire_tests}/${test%.txt}+A.txt ${repertoire_tests}/${test%.txt}+B1.txt ${test%.txt}+B1.txt`
	            else
	                diff -tibw "${repertoire_tests}/${test%.txt}+B1.txt" "${test%.txt}+B1.txt" 2>&1 > /dev/null
	                if [ $? -eq 0 ]; then validB1="=="; else validB1="!="; fi
	            fi
            fi
            if ( [ -e ${repertoire_tests}/${test%.txt}+A.txt ] && [ -e ${repertoire_tests}/${test%.txt}+B2.txt ] ) ; then
                if( [ -x ${valideurB} ]  ) ; then
	                validB2=`$valideurB -c ${repertoire_tests}/${test} ${repertoire_tests}/${test%.txt}+A.txt ${repertoire_tests}/${test%.txt}+B2.txt ${test%.txt}+B2.txt`
	            else
	                diff -tibw "${repertoire_tests}/${test%.txt}+B2.txt" "${test%.txt}+B2.txt" 2>&1 > /dev/null
	                if [ $? -eq 0 ]; then validB2="=="; else validB2="!="; fi
	            fi
            fi
            if ( [ -e ${repertoire_tests}/${test%.txt}+A.txt ] && [ -e ${repertoire_tests}/${test%.txt}+B3.txt ] ) ; then
                if( [ -x ${valideurB} ]  ) ; then
	                validB3=`$valideurB -c ${repertoire_tests}/${test} ${repertoire_tests}/${test%.txt}+A.txt ${repertoire_tests}/${test%.txt}+B3.txt ${test%.txt}+B3.txt`
	            else
	                diff -tibw "${repertoire_tests}/${test%.txt}+B3.txt" "${test%.txt}+B3.txt" 2>&1 > /dev/null
	                if [ $? -eq 0 ]; then validB3="=="; else validB3="!="; fi
	            fi            
            fi
            
            echo -e "${test%.txt}\t${tA}\t${validA}\t${tB1}\t${validB1}\t${tB2}\t${validB2}\t${tB3}\t${validB3}"
            echo -e "${test%.txt}\t${tA}\t${validA}\t${tB1}\t${validB1}\t${tB2}\t${validB2}\t${tB3}\t${validB3}" >> $logfile
            
            sommaire="${sommaire}\t${tA}\t${validA}\t${tB1}\t${validB1}\t${tB2}\t${validB2}\t${tB3}\t${validB3}"
        done
    done
}

# Lister les soumissions Oto (répertoires terminant par .tp_oto)
tps=`ls -1 | grep .tp_oto`
# Si aucun répertoire .tp_oto n'existe, essayer le répertoire courant (auto-évaluation)
if [ ! -n "$tps" ]; then
    tps="."
fi

# Génération de l'entête du rapport
echo "#Rapport de correction INF3105 / TP2" > "rapport-${date}.txt"
echo -e "#Date:\t${date}" >> "rapport-${date}.txt"
echo -e "#Machine :\t" `hostname` >> "rapport-${date}.txt"
echo -e "#CPU :\t$cpuinfo" >> "rapport-${date}.txt"
echo >> "rapport-${date}.txt"


# Génération des titres des colonnes
echo -n -e "Soumission\t__H\tSTL\tTaille" >> "rapport-${date}.txt"
for carte in $cartes;
do
    tests="`cd $repertoire_tests && ls ${carte}-electeurs[0-9].txt`"
    for test in $tests;
    do
        echo -n -e "\t$test\t\t\t\t\t\t\t\t" >> "rapport-${date}.txt"
    done
done
echo >> "rapport-${date}.txt"


echo -n -e "Soumission\t__H\tSTL\tTaille" >> "rapport-${date}.txt"
# Itérer sur toutes les cartes
for carte in $cartes;
do
    # Lister les fichiers tests
    tests="`cd $repertoire_tests && ls ${carte}-electeurs[0-9].txt`"
    for test in $tests;
    do
        echo -n -e "\tA_CPU\tA_NbOpt\tTaille\tB1_CPU\tB1_Nb\tB2_CPU\tB2_Nb\tB3_CPU\tB3_Nb" >> "rapport-${date}.txt"
    done
done
echo >> "rapport-${date}.txt"

for tp in $tps;
do
    sommaire=""
    echo "## CORRECTION : $tp"
    pushd $tp
        EvaluerTP
#       Nettoyer
    popd
    #echo -e ">> ${sommaire}"
    echo -e "${tp}\t${sommaire}" >> "rapport-${date}.txt"
done

