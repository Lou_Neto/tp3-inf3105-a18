/*  INF3105 - Structures de données et algorithmes
    UQAM | Faculté des sciences | Département d'informatique
    TP3
    Fichier de départ carte.h © Éric Beaudry (beaudry.eric@uqam.ca).
    http://ericbeaudry.ca/INF3105/tp3/

    Lou-Gomes Neto (NETL14039105)
*/
#if !defined(_CARTE__H_)
#define _CARTE__H_
#include <istream>
#include <list>
#include <set>
#include <map>
#include <string>
#include <vector>
#include "pointst.h"

class Carte{
  public:
    void ajouter_noeud(long id, const PointST& c);
    void ajouter_route(const std::string& nom, 
                       const std::list<long>& noms);

    double calculerChemin(const std::string& nomelecteur, 
                          const std::string& origine, 
                          const std::string& destination,
                          std::list<std::string>& out_cheminnoeuds, 
                          std::list<std::string>& out_cheminroutes);
    
    int calculerPancartes(int nbPancartes, 
                          std::vector<std::pair<std::string, 
                          std::string> >& solution);

  private:
    struct Noeud{
      PointST point;
      std::set<long> voisins; 
    };
    std::map<long, Noeud> noeuds;
    std::vector<std::string> nomRoutes;
    std::vector<std::list<long> > routes;

    struct compare 
    {
      bool operator()(const std::pair<double, long>& p1, 
                      const std::pair<double, long>& p2)
      {
        return p1.first > p2.first;
      }
    };
};

// Déclaration opérateur (fonction) global
std::istream& operator >> (std::istream& is, Carte& carte);

#endif
